'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version',
  'myAppControllers',
  'ui.bootstrap'
])
.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
	$routeProvider
	.when('/view1', {
		templateUrl: 'view1/coffees.html',
		controller: 'CoffeesCtrl'
  	})
	.when('/view2/:id', {
		templateUrl: 'view2/reviews.html',
		controller: 'ReviewsCtrl'
  	});
  $routeProvider.otherwise({redirectTo: '/view1'});
}])

